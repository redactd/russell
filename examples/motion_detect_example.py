#
# Russell PIN/SAO/Badge v1.4
# 2021 - redactd
#
# Thanks to @IObrizio for the guidance
#
# #badgelife
#
# for firmware, code examples
# https://gitlab.com/redactd/russell
import time
import time
import board
from digitalio import DigitalInOut, Direction, Pull
import neopixel

print("                      				    ")
print("                      				    ")
print("    /)/)              				    ")
print("   ( ..\              				    ")
print("   /'-._)             				    ")
print("  /#/                 				    ")
print(" /#/  russell 1.4     				    ")
print("      pcb by redactd 				    ")
print("                      				    ")
print("      special thanks to @IObrizio		")
print("")
print("")
print("")


#debug messages?
debug = True        # normal level of debug messages 
verbose = True     # probably too much info

# define eye LED.
pixel_pin = board.LED
num_pixels = 1
pixels = neopixel.NeoPixel(pixel_pin,
                            num_pixels,
                            brightness=.2,
                            auto_write=False)

# define neo ring.
neoboard = board.D27
neoboard_num_pixels = 36
nb_pixels = neopixel.NeoPixel(neoboard,
                            neoboard_num_pixels,
                            brightness=.02,
                            auto_write=False)



number_of_rainbow_cycles = 2

# define the GPIO pins
gpio3 = DigitalInOut(board.D3) # gpio located on nose
gpio3.direction = Direction.INPUT

#gpio27 = DigitalInOut(board.D27) # gpio locate on neck
#gpio27.direction = Direction.INPUT


# define the buttons.
switch2 = DigitalInOut(board.D15) # this button changes the modes
switch2.direction = Direction.INPUT
switch2.pull = Pull.UP
switch2_prev_state = switch2.value

switch = DigitalInOut(board.D16) # this button changes the colors for mode 'color'
switch.direction = Direction.INPUT
switch.pull = Pull.UP
switch_prev_state = switch.value


# common colors
RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)

# vars for button press logic

modes = ['solid','rainbow','off']
max_modes = len(modes) -1

levels = {}
levels['solid'] = [RED,BLUE,YELLOW,GREEN,CYAN,PURPLE]
levels['rainbow'] = [.01] # speed
levels['off'] = [1]

current_mode = 0
current_mode_level = 0

prev_switch_value = None
prev_switch2_value = None

def do_work():
    if verbose: print("*"*10,   "doing work")
    
    if modes[current_mode] == 'solid':
        color = levels[modes[current_mode]][current_mode_level]
        if verbose: print("*"*10,   "setting color:",color)
        pixels.fill(color)
        pixels.show()
    elif modes[current_mode] =='rainbow':
        if verbose: print("*"*10,   "doing rainbow",number_of_rainbow_cycles,"times")
        for i in range(0,number_of_rainbow_cycles): # how many time to cycle rainbow?
            rainbow_cycle(levels[modes[current_mode]][current_mode_level])
            time.sleep(1)
            if verbose: print("*"*10,   "done with rainbow",i+1, "of",number_of_rainbow_cycles,"total")
        if verbose: print("*"*10,   "done doing rainbow")
    if modes[current_mode] == 'off':
        if verbose: print("*"*10,   "LED OFF")
        color = (0,0,0)
        pixels.fill(color)
        pixels.show()

######
def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        return (0, 0, 0)
    if pos < 85:
        return (255 - pos * 3, pos * 3, 0)
    if pos < 170:
        pos -= 85
        return (0, 255 - pos * 3, pos * 3)
    pos -= 170
    return (pos * 3, 0, 255 - pos * 3)
def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            rc_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(rc_index & 255)
        pixels.show()
        time.sleep(wait)
def flash():
    for i in range(0,3):
        pixels.fill(RED)
        nb_pixels.fill(RED)
        pixels.show()
        nb_pixels.show()
        time.sleep(.1)
        pixels.fill((0,0,0))
        nb_pixels.fill((0,0,0))
        pixels.show()
        nb_pixels.show()
        time.sleep(.1)
######


#boot up rainbow.
if verbose: print("*"*10,   "doing boot rainbow")
for x in range(0,2):
    rainbow_cycle(0)

# idle state, show red.
if verbose: print("*"*10,   "setting initial LED to RED")
pixels.fill(RED)
pixels.show()

for p in range(0,neoboard_num_pixels):
    nb_pixels[p]=(BLUE)
    nb_pixels.show()
    time.sleep(.01)

time.sleep(1)
nb_pixels.fill((0,0,0))
nb_pixels.show()



#do_work() # setting led red instead of entering do work.
ignore_motion = False # used to delay motion detect after its triggered.
motion_count = 0

while True:

    cur_switch_value = switch.value
    cur_switch2_value = switch2.value

    if gpio3.value:
    
        if not ignore_motion:
            if debug: print("*"*10,   "MOTION DETECTED!")
            ignore_motion = True
            flash()
            do_work()
        
    if not switch.value: 
        if cur_switch_value != prev_switch_value:
            if debug: print("*"*10,   "switch press")
            if current_mode < max_modes: current_mode+=1
            else:current_mode =0
            
            current_mode_level = 0
            
            #flash red LED when mode change.
            pixels.fill(RED)
            pixels.show()
            time.sleep(.1)
            pixels.fill((0,0,0))
            pixels.show()
            
            # call into do work to find whats next
            do_work()
            
        time.sleep(.1)
        prev_switch_value = switch.value

    elif not switch2.value:
        if cur_switch2_value != prev_switch2_value:
            if debug: print("*"*10,   "switch 2 press")
            if current_mode_level < (len(levels.get(modes[current_mode]))-1):
                current_mode_level+=1
            else:current_mode_level =0
            do_work()
        time.sleep(.1)
        prev_switch2_value = switch2.value
    else:
        # handle motion detector status
        if ignore_motion:
            if motion_count<30:motion_count+=1
            else:
                motion_count=0
                ignore_motion=False
                
                
        time.sleep(.1)
        pass
